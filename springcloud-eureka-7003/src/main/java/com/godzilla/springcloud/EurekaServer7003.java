package com.godzilla.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Eureka 集群环境搭建
 * @author STRIXSCAR3
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServer7003 {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServer7003.class, args);
    }

}
