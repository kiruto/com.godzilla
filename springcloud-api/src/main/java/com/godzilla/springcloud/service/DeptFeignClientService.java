package com.godzilla.springcloud.service;

import com.godzilla.springcloud.pojo.Dept;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 远程过程调用负载均衡 Feign 接口
 *  name：注册微服务名（id）
 *  fallbackFactory：服务降级实现类
 * 调用目前有两种：
 *  1、接口提供方在注册中心：
 *      name = "SPRING-PROVIDER-DEPT", fallbackFactory = DeptClientHystrixFallbackFactory.class
 *  2、单独的一个http接口，接口提供方没有注册到注册中心：
 *      name="xxx",url="localhost:8001"，name可以为注册中心的实例名称，加上url属性时，name的值就与注册中心实例名称无关
 * @author STRIXSCAR3
 */
@FeignClient(name = "SPRING-PROVIDER-DEPT", fallbackFactory = DeptClientHystrixFallbackFactory.class)
@Component
public interface DeptFeignClientService {

    /**
     * 添加部门是否成功
     * @return boolean
     */
    @PostMapping("/dept/add")
    boolean addDept(Dept dept);

    /**
     * 通过 id 查询部门
     * @param id 部门id
     * @return Dept
     */
    @GetMapping("/dept/get/{id}")
    Dept queryById(@PathVariable(value = "id") Long id);

    /**
     * 查询全部部门
     * @return List<Dept>
     */
    @RequestMapping("/dept/list")
    List<Dept> queryAll();

}
