package com.godzilla.springcloud.service;

import com.godzilla.springcloud.pojo.Dept;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Hystrix 服务降级，将返回整个 feign DeptFeignClientService 服务
 * @author kiruto
 */
@Component
public class DeptClientHystrixFallbackFactory implements FallbackFactory {

    /**
     * 服务熔断和降级的区别：
     *  服务熔断 —> 服务端：某个服务超时或异常，引起熔断，类似于保险丝(自我熔断)
     *  服务降级 —> 客户端：从整体网站请求负载考虑，当某个服务熔断或者关闭之后，服务将不再被调用，此时在客户端，我们可以准备一个 FallBackFactory
     *             返回一个默认的值(缺省值)。会导致整体的服务下降，但是好歹能用，比直接挂掉强。
     *  触发原因不太一样，服务熔断一般是某个服务（下游服务）故障引起，而服务降级一般是从整体负荷考虑；
     *  管理目标的层次不太一样，熔断其实是一个框架级的处理，每个微服务都需要（无层级之分），而降级一般需要对业务有层级之分（比如降级一般是从最外围服务开始）
     *  实现方式不太一样，服务降级具有代码侵入性(由控制器完成/或自动降级)，熔断一般称为自我熔断
     */
    @Override
    public DeptFeignClientService create(Throwable throwable) {

        return new DeptFeignClientService() {
            @Override
            public boolean addDept(Dept dept) {
                return false;
            }

            @Override
            public Dept queryById(Long id) {
                return new Dept().
                        setDeptno(id).
                        setDname("id=>" + id + "=>户信息无法查询到!--@Hystrix客户端服务降级信息--该服务已关闭！").
                        setDb_source("Can Not Connect To Database!");
            }

            @Override
            public List<Dept> queryAll() {
                Dept dept = new Dept().
                        setDeptno(0).
                        setDname("=>户信息无法查询到!--@Hystrix客户端服务降级信息--该服务已关闭！").
                        setDb_source("Can Not Connect To Database!");
                ArrayList<Dept> deptArrayList = new ArrayList<>();
                deptArrayList.add(dept);
                return deptArrayList;
            }
        };
    }

}
