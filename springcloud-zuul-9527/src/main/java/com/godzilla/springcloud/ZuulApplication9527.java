package com.godzilla.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author STRIXSCAR3
 */
@SpringBootApplication
@EnableZuulProxy
public class ZuulApplication9527 {

    public static void main(String[] args) {
        SpringApplication.run(ZuulApplication9527.class, args);
    }

}
