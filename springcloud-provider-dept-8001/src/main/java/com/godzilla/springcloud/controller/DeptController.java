package com.godzilla.springcloud.controller;

import com.godzilla.springcloud.pojo.Dept;
import com.godzilla.springcloud.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 提供 Restful 服务
 * @author STRIXSCAR3
 */
@RestController
public class DeptController {

    @Autowired
    private DeptService deptService;
    /**
     * 获取一些注册的信息，得到具体的微服务
     */
    @Autowired
    private DiscoveryClient discoveryClient;

    @PostMapping("/dept/add")
    public boolean addDept(Dept dept) {
        return deptService.addDept(dept);
    }

    @GetMapping("/dept/get/{id}")
    public Dept get(@PathVariable("id") Long id) {
        return deptService.queryById(id);
    }

    @GetMapping("/dept/list")
    public List<Dept> queryAll() {
        return deptService.queryAll();
    }

    /**
     * 注册进来的微服务，获取一些信息
     * @return
     */
    @GetMapping("/dept/discover")
    public Object discoveryClientInfo() {
        // 获取微服务列表的清单
        List<String> services = discoveryClient.getServices();
        System.out.println("注冊服务：" + services);

        // 得到一个具体的微服务信息，通过具体的微服务id -> spring.application.name
        List<ServiceInstance> instances = discoveryClient.getInstances("SPRING-PROVIDER-DEPT");
        for (ServiceInstance instance : instances) {
            System.out.println(
                    instance.getHost() + "\t" +
                    instance.getPort() + "\t" +
                    instance.getUri() + "\t" +
                    instance.getServiceId()
            );
        }
        return this.discoveryClient;
    }

}
