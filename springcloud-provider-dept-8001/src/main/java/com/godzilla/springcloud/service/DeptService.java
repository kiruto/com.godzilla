package com.godzilla.springcloud.service;

import com.godzilla.springcloud.pojo.Dept;

import java.util.List;

/**
 * @author Godzilla
 */
public interface DeptService {

    /**
     * 添加部门是否成功
     * @param dept
     * @return
     */
    public boolean addDept(Dept dept);

    /**
     * 通过 id 查询部门
     * @param id
     * @return
     */
    public Dept queryById(Long id);

    /**
     * 查询全部部门
     * @return
     */
    public List<Dept> queryAll();

}
