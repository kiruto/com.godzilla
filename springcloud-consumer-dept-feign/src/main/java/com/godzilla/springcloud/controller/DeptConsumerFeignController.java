package com.godzilla.springcloud.controller;

import com.godzilla.springcloud.pojo.Dept;
import com.godzilla.springcloud.service.DeptFeignClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 消费者控制器
 * @author STRIXSCAR3
 */
@RestController
public class DeptConsumerFeignController {

    /**
     * 将 springcloud-api 中的 Feign DeptFeignClientService 注入
     */
    @Autowired
    private DeptFeignClientService deptFeignClientService;

    @PostMapping("/dept/add")
    public boolean addDept(Dept dept) {
        return this.deptFeignClientService.addDept(dept);
    }

    @GetMapping("/dept/get/{id}")
    public Dept get(@PathVariable (value = "id") Long id) {
        return this.deptFeignClientService.queryById(id);
    }

    @RequestMapping("/dept/list")
    public List<Dept> queryAll() {
        return this.deptFeignClientService.queryAll();
    }

}
