package com.godzilla.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * 配置类
 * @author STRIXSCAR3
 */
@Configuration
public class ConfigBean {

    /**
     * 访问 restful 请求的 RestTemplate，配置负载均衡实现 RestTemplate
     * IRule 所有负载均衡策略的父接口，里边的核心方法就是choose方法，用来选择一个服务实例：
     *      AvailabilityFilteringRule：会先过滤掉跳闸、访问鼓掌的服务，对剩下的进行轮询
     *      RoundRobinRule：轮询
     *      RandomRule：随机算法，随机选择一个服务实例
     *      WeightedResponseTimeRule：每30秒计算一次服务器响应时间，以响应时间作为权重，响应时间越短的服务器被选中的概率越大
     *      RetryRule：
     * @LoadBalanced 启用 Ribbon 的注解
     * @return
     */
    @Bean
    @LoadBalanced // Ribbon
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

}
