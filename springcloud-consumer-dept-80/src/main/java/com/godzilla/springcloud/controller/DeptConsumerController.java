package com.godzilla.springcloud.controller;

import com.godzilla.springcloud.pojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * 消费者控制器
 * @author STRIXSCAR3
 */
@RestController
public class DeptConsumerController {

    /**
     * ConfigBean 中交给容器管理的 RestTemplate
     */
    @Autowired
    private RestTemplate restTemplate;

    /**
     * provider-dept-8001 请求前缀
     * 当启用了 Ribbon 后，这里的地址应该是一个变量，不应该再是写死的地址了
     * 使用 springcloud-provider-dept-8001 在 eureka 注册的服务名即可：
     * spring:
     *   application:
     *     name: spring-provider-dept
     */
//    private static final String REST_URL_PREFIX = "http://localhost:8001";
    private static final String REST_URL_PREFIX = "http://SPRING-PROVIDER-DEPT";

    /**
     * 无 Service 层，使用 RestTemplate 远程调用服务
     * 理解：消费者不应该有 Service 层
     * RestTemplate：传统情况下在 java 代码里访问 restful 服务，一般使用 Apache 的 HttpClient，不过此种方法使用起来太过繁琐
     *               Spring 提供了一种简单便捷的模板类来进行操作(提供多种便捷方式访问远程 HTTP 服务)，这就是 RestTemplate
     * String url：请求路径
     * @Nullable Object request：请求参数
     * Class<T> responseType：响应数据类型
     */
    @RequestMapping("consumer/dept/add")
    public boolean addDept(Dept dept) {
        restTemplate.postForObject(REST_URL_PREFIX + "/dept/add", dept, Boolean.class);
        return false;
    }

    @RequestMapping("consumer/dept/get/{id}")
    public Dept get(@PathVariable (value = "id") Long id) {
        return restTemplate.getForObject(REST_URL_PREFIX + "/dept/get/" + id, Dept.class);
    }

    @RequestMapping("consumer/dept/list")
    public List<Dept> queryAll() {
        return restTemplate.getForObject(REST_URL_PREFIX + "/dept/list", List.class);
    }

}
