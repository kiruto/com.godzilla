package com.godzilla.springcloud;

import com.godzilla.myrRule.MyRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

/**
 * Ribbon 和 Eureka 整合以后，客户端可以直接用服务名调用，不用关心 ip 地址和端口号
 * 消费者主启动类
 * @author STRIXSCAR3
 */
@SpringBootApplication
@EnableEurekaClient
@RibbonClient(name = "SPRING-PROVIDER-DEPT", configuration = MyRule.class)
public class DeptConsumer80 {

    public static void main(String[] args) {
        SpringApplication.run(DeptConsumer80.class, args);
    }

}
