package com.godzilla.myrRule;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.Server;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 自定义负载均衡算法 demo
 * 自定义 Ribbon 客户端不应在主程序上下文的 @ComponentScan 中，否则将由所有 @RibbonClients 共享
 * 如果您使用 @Component 或 @SpringBootApplication,则需采取措施避免包含（例如将其放在一个单独的，不重叠的包中，或者指定要在 @ComponentScan）
 * @author STRIXSCAR3
 */
public class MyLoadBalenced extends AbstractLoadBalancerRule {

    private int total = 0;
    private int currentIndex = 0;

    /**
     * 改写 Ribbon 负载均衡算法，每个服务访问5次，换下一个服务
     * total：服务访问次数
     * currentIndex：当前服务下标
     * @param lb
     * @param key
     * @return
     */
    public Server choose(ILoadBalancer lb, Object key) {
        if (lb == null) {
            return null;
        }
        Server server = null;
        while (server == null) {
            if (Thread.interrupted()) {
                return null;
            }
            // 获得活着的服务
            List<Server> upList = lb.getReachableServers();
            // 获得全部的服务
            List<Server> allList = lb.getAllServers();
            int serverCount = allList.size();
            if (serverCount == 0) {
                return null;
            }
            // 生成区间随机数
//            int index = chooseRandomInt(serverCount);
            // 随机获取一个服务
//            server = upList.get(index);
            if (total < 5) {
                server = upList.get(currentIndex);
                total++;
            } else {
                total = 0;
                currentIndex++;
                if (currentIndex > upList.size()) {
                    currentIndex = 0;
                }
                server = upList.get(currentIndex);
            }
            System.out.println("第几次：" + total);

            if (server == null) {
                Thread.yield();
                continue;
            }
            if (server.isAlive()) {
                return (server);
            }
            server = null;
            Thread.yield();
        }

        return server;

    }

    protected int chooseRandomInt(int serverCount) {
        return ThreadLocalRandom.current().nextInt(serverCount);
    }

    @Override
    public Server choose(Object key) {
        return choose(getLoadBalancer(), key);
    }

    @Override
    public void initWithNiwsConfig(IClientConfig clientConfig) {

    }

}
