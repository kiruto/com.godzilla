package com.godzilla.myrRule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自定义 Ribbon 负载均衡方式
 * @author STRIXSCAR3
 */
@Configuration
public class MyRule {

    /**
     * 默认是轮询，现在启用我们自己定义的算法
     * @return
     */
    @Bean
    public IRule ribbonRule() {
//        return new MyLoadBalenced();
        return new RoundRobinRule();
    }

}
