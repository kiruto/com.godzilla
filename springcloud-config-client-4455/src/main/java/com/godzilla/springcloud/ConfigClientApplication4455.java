package com.godzilla.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author STRIXSCAR3
 */
@SpringBootApplication
public class ConfigClientApplication4455 {

    public static void main(String[] args) {
        SpringApplication.run(ConfigClientApplication4455.class, args);
    }

}
