package com.godzilla.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试客户端连接服务端，读取远程配置
 * @author STRIXSCAR3
 */
@RestController
public class ConfigClientController {

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${server.port}")
    private String port;

    @Value("${eureka.client.service-url.defaultZone}")
    private String eureka;

    @GetMapping("/config")
    public String getConfig() {
        return "applicationName:"+ applicationName + ",\n"
                +"port:"+ port + ",\n"
                +"eureka:"+ eureka + "\n";
    }

}
