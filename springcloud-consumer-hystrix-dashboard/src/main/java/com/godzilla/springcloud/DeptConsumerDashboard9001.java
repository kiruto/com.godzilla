package com.godzilla.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * EnableHystrixDashboard 注解 开启 HystrixDashboard 服务熔断监控面板（服务端需要 actuator 依赖）
 * @author STRIXSCAR3
 */
@SpringBootApplication
@EnableHystrixDashboard
public class DeptConsumerDashboard9001 {

    public static void main(String[] args) {
        SpringApplication.run(DeptConsumerDashboard9001.class, args);
    }

}
