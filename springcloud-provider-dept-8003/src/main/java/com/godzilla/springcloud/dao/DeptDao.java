package com.godzilla.springcloud.dao;

import com.godzilla.springcloud.pojo.Dept;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author STRIXSCAR3
 */
@Mapper
@Repository
public interface DeptDao {

    /**
     * 添加部门是否成功
     * @param dept
     * @return
     */
    public boolean addDept(Dept dept);

    /**
     * 通过 id 查询部门
     * @param id
     * @return
     */
    public Dept queryById(Long id);

    /**
     * 查询全部部门
     * @return
     */
    public List<Dept> queryAll();

}
