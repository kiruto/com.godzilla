package com.godzilla.springcloud.controller;

import com.godzilla.springcloud.pojo.Dept;
import com.godzilla.springcloud.service.DeptService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @HystrixCommand 添加对 Hystrix 的支持
 * @author STRIXSCAR3
 */
@RestController
public class DeptController {

    @Autowired
    private DeptService deptService;

    /**
     * @HystrixCommand 熔断、服务降级 (注解是方法级别的，在你需要捕获的方法上加上注解：ElementType.METHOD)
     *  fallbackMethod：调用服务异常执行方法
     * @param id
     * @return
     */
    @HystrixCommand(fallbackMethod = "hystrixGet")
    @GetMapping("dept/get/{id}")
    public Dept get(@PathVariable(value = "id") long id) {
        Dept dept = deptService.queryById(id);
        if (dept == null) {
            throw new RuntimeException("id=>" + id + "=>不存在该用户，或用户信息无法查询到!");
        }
        return dept;
    }

    /**
     * get()方法失败后的备选方法
     * @param id
     * @return
     */
    public Dept hystrixGet(@PathVariable(value = "id") long id) {
        return new Dept().
                setDeptno(id).
                setDname("id=>" + id + "=>不存在该用户，或用户信息无法查询到!--@Hystrix").
                setDb_source("No Such Database In MySql!");
    }

}
