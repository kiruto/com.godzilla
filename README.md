# com.godzilla

#### 介绍
     SpringCloud 分布式微服务，核心五大组件，整个远程配置中心基础入门项目

#### 软件架构
     SpringCloud + Eureka + Ribbon + Feign + Hystrix + Zuul + Config + MyBatis

### 模块
    
-     Parent-module：springcloud
-     Module：springcloud-api    公共实体、服务
-     Module：springcloud-consumer-dept-80    服务消费者
-     Module：springcloud-consumer-dept-feign    整合 Feign 服务消费者
-     Module：springcloud-consumer-hystrix-dashboard    整合 Hystrix 流监控
-     Module：springcloud-eureka-7001    Eureka注册中心集群（端口：7001）
-     Module：springcloud-eureka-7002    Eureka注册中心集群（端口：7002）
-     Module：springcloud-eureka-7003    Eureka注册中心集群（端口：7003）
-     Module：springcloud-provider-dept-8001    服务提供者（端口：8001）
-     Module：springcloud-provider-dept-8002    服务提供者（端口：8002）
-     Module：springcloud-provider-dept-8003    服务提供者（端口：8003）
-     Module：springcloud-provider-dept-hystrix-8001    整合 Hystrix 服务提供者（端口：8001）
-     Module：springcloud-zuul-9527    整合 Zuul 路由网关
-     Module：springcloud-config-server-3344    整合 Spring Cloud Config 分布式远程配置中心 - 服务端
-     Module：springcloud-config-client-4455    整合 Spring Cloud Config 分布式远程配置中心 - 客户端

### 安装使用
    复制地址，Clone 到本地，导入开发工具，直接运行